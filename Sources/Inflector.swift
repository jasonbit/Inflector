//
// Inflector.swift
// Copyright (c) 2014 NGeen
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import Foundation

private let instance = Inflector()
private var pluralRules = [InflectorRule]()
private var singularRules = [InflectorRule]()
private var words = NSMutableSet()

public class Inflector {
 
     private init() {
        setupInflectionRules()
    }

    public class func sharedInstance() -> Inflector {
        return instance
    }
  
    /**
    * The function add the rules for the inflection
    *
    *
    */

    private func setupInflectionRules() {
        for value in InflectionRules.irregularRules as [[String]] {
            setPluralRule(value[0] as String, forReplacement: value[1] as String)
        }
        for value in InflectionRules.pluralRules as [[String]] {
            setPluralRule(value[0] as String, forReplacement: value[1] as String)
        }
        for value in InflectionRules.irregularRules as [[String]] {
            setSingularRule(value[1] as String, forReplacement: value[0] as String)
        }
        for value in InflectionRules.singularRules as [[String]] {
            setSingularRule(value[0] as String, forReplacement: value[1] as String)
        }    
        for value in InflectionRules.uncountableWords as [String] {
            setUncountableWord(value)
        }
    }

    /**
    * The function return the pluralized string.
    *
    * @param string The string to pluralize.
    *
    * return String
    */
    
    public func pluralize(string: String) -> String {
        return applyRules(pluralRules, forString: string)
    }
    
    /**
    * The function add a new irregular rule.
    *
    * @param singular The singular rule to apply.
    * @param plural The plural rule to apply.
    *
    */
    
    private func setIrregularRuleForSingular(singular: String, andPlural plural: String) {
        let singularRule: String = "\(plural)$"
        setSingularRule(singularRule, forReplacement: singular)
        let pluralRule: String = "\(singular)$"
        setPluralRule(pluralRule, forReplacement: plural)
    }
    
    /**
    * The function add a new singular rule.
    *
    * @param rule The rule to apply.
    * @param replacement The value to replace the rule.
    *
    */
    
    public func setSingularRule(rule: String, forReplacement replacement: String) {
        singularRules.append(InflectorRule(rule: rule, replacement: replacement))
    }
    
    /**
    * The function add a new plural rules.
    *
    * @param rule The rule to apply.
    * @param replacement The value to replace the rule.
    *
    */
    
    public func setPluralRule(rule: String, forReplacement replacement: String) {
        pluralRules.append(InflectorRule(rule: rule, replacement: replacement))
    }
    
    /**
    * The function add the a new uncountable word
    *
    * @param word The uncountable word.
    *
    */
    
    public func setUncountableWord(word: String) {
        words.addObject(word)
    }
    
    /**
    * The function return the singularized string.
    *
    * @param string The string to singularize
    .
    *
    * return String
    */
    
    public func singularize(string: String) -> String {
        return applyRules(singularRules, forString: string)
    }
    
//MARK: Private methods
    
    /**
    * The function apply the inflection rules to a given string
    *
    * @param rules The array with rules to apply.
    * @param string The string to apply the rules.
    *
    */
    
    private func applyRules(rules: [InflectorRule], forString string: String) -> String {
        if words.containsObject(string) {
            return string
        // } else if self.irregularRules.containsObject(string) {
        //     return string
        } else {
            for rule in rules {
                let range = NSMakeRange(0, string.lengthOfBytesUsingEncoding(NSUTF8StringEncoding))
                do {
                    let regex: NSRegularExpression = try NSRegularExpression(pattern: rule.rule, options: NSRegularExpressionOptions.CaseInsensitive)
                    if regex.firstMatchInString(string, options: NSMatchingOptions.ReportProgress, range: range) != nil {
                        return regex.stringByReplacingMatchesInString(string, options: NSMatchingOptions.ReportProgress, range: range, withTemplate: rule.replacement)
                    }
                } catch {
                    print("Error creating regex: \(error)")
                }
            }
        }
        return string
    }
}
